package com.atlassian.stash.plugins.jirahook;

import com.atlassian.applinks.api.*;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.content.ChangesetCallback;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.idx.ChangesetIndex;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.RepositoryService;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.sal.api.net.Request.MethodType.PUT;

public class JiraHook implements PreReceiveRepositoryHook {

    private static final Pattern ISSUE_KEY = Pattern.compile("((?<!([A-Z]{1,10})-?)[A-Z]+-\\d+)");

    private static final Pattern DIRECTIVE = Pattern.compile("#(assign) ((?:\")[^\"]+(?:\")|[^\\s]+)");
    private static final int OP = 1;
    private static final int ARG = 2;

    private static final String PROCESSED = "jira.directives.processed";
    private static final Collection<String> PROCESSED_SET = Sets.newHashSet(PROCESSED);

    private final ChangesetIndex changesetIndex;   // TODO is storing the fact that these directives have been processed in the ChangesetIndex really appropriate??
    private final HistoryService historyService;
    private final ApplicationLinkService applicationLinkService;
    private final JiraHookService jiraHookService;
    private final UserManager userManager;
    private final ApplicationProperties applicationProperties;
    private final RepositoryService repositoryService;
    private final TransactionTemplate txTemplate;

    public JiraHook(ChangesetIndex changesetIndex, HistoryService historyService,
                    ApplicationLinkService applicationLinkService, JiraHookService jiraHookService,
                    UserManager userManager, ApplicationProperties applicationProperties,
                    RepositoryService repositoryService, TransactionTemplate txTemplate) {
        this.changesetIndex = changesetIndex;
        this.historyService = historyService;
        this.applicationLinkService = applicationLinkService;
        this.jiraHookService = jiraHookService;
        this.userManager = userManager;
        this.applicationProperties = applicationProperties;
        this.repositoryService = repositoryService;
        this.txTemplate = txTemplate;
    }

    @Override
    public boolean onReceive(@Nonnull final RepositoryHookContext hookContext, @Nonnull Collection<RefChange> refChanges,
                             @Nonnull final HookResponse hookResponse) {
        final ApplicationLink jiraLink = applicationLinkService.getPrimaryApplicationLink(JiraApplicationType.class);
        if (jiraLink == null) {
            // no JIRA server to update
            return true;
        }

        final String username = userManager.getRemoteUsername();
        if (username == null) {
            // anon not supported
            return true;
        }

        if (!jiraHookService.isEnabled(username)) {
            // disabled
            hookResponse.out().write("\n\tJIRA directives disabled for this push.\n\n");
            return true;
        }

        final Map<String, String> assigned = Maps.newHashMap();
        final Map<String, String> failed = Maps.newHashMap();

        final Set<String> errorMessages = Sets.newLinkedHashSet();

        for (RefChange refChange : refChanges) {
            historyService.streamChangesetsBetween(hookContext.getRepository(), refChange.getFromHash(),
                    refChange.getToHash(), null, new ChangesetCallback() {

                @Override
                public boolean onChangeset(final Changeset changeset) throws IOException {

                    Set<String> processed = changesetIndex.getAttributeValues(changeset.getId(), PROCESSED_SET).get(PROCESSED);
                    if (!(processed == null || processed.isEmpty())) { // TODO fuzzy contract
                        // already processed, continue
                        return true;
                    }

                    Matcher keyMatcher = ISSUE_KEY.matcher(changeset.getMessage());
                    if (!keyMatcher.find()) {
                        // no issue key, continue
                        return true;
                    }

                    final String issueKey = keyMatcher.group(0);

                    if (assigned.containsKey(issueKey) || failed.containsKey(issueKey)) {
                        // a later commit already attempted to update the assignee, continue
                        return true;
                    }

                    Matcher directiveMatcher = DIRECTIVE.matcher(changeset.getMessage());
                    while (directiveMatcher.find()) {
                        // only op is assign atm
                        final String assignee = directiveMatcher.group(ARG);

                        boolean issueAssigned = false;
                        try {
                            final ApplicationLinkRequestFactory requestFactory = jiraLink.createAuthenticatedRequestFactory();

                            issueAssigned = requestFactory
                                .createRequest(PUT, String.format("rest/api/2/issue/%s/assignee", issueKey))
                                .addHeader("Content-Type", "application/json")
                                .setEntity(new JiraAssignee(assignee))
                                .execute(new ApplicationLinkResponseHandler<Boolean>() {
                                    @Override
                                    public Boolean credentialsRequired(Response response) throws ResponseException {
                                        hookResponse.out().write("Please visit:\n\n\t" + requestFactory.getAuthorisationURI() +
                                                "\n\nto grant authorize Stash to update JIRA on your behalf.\n");
                                        return false;
                                    }

                                    @Override
                                    public Boolean handle(Response response) throws ResponseException {
                                        if (response.isSuccessful()) {
                                            return true;
                                        } else {
                                            try {
                                                JiraErrors jiraErrors = response.getEntity(JiraErrors.class);
                                                for (String errorMessage : jiraErrors.getErrorMessages()) {
                                                    errorMessages.add(errorMessage + " (" + issueKey + ")");
                                                }
                                                for (Map.Entry<String, String> entry : jiraErrors.getErrors().entrySet()) {
                                                    errorMessages.add("(" + entry.getKey() + ") " + entry.getValue());
                                                }
                                            } catch (Exception e) {
                                                errorMessages.add(response.getStatusText() + ": " + response.getResponseBodyAsString());
                                            }
                                            return false;
                                        }
                                    }
                                });
                        } catch (CredentialsRequiredException e) {
                            hookResponse.out().write("Please visit:\n\n\t" + e.getAuthorisationURI() +
                                    "\n\nto grant authorize Stash to update JIRA on your behalf.\n");
                        } catch (ResponseException e) {
                            hookResponse.out().write(String.format("Failed to assign %s!\n%s\n", issueKey, e.getMessage()));
                        }

                        if (issueAssigned) {
                            failed.remove(issueKey);
                            assigned.put(issueKey, assignee);
                            if (changesetIndex.getChangeset(changeset.getId()) == null) {
                                txTemplate.execute(new TransactionCallback<Void>() {
                                    @Override
                                    public Void doInTransaction() {
                                        // avoid "detached entity passed to persist" issues TODO DEMO IN THIRTY MINS ;)
                                        Integer repositoryId = hookContext.getRepository().getId();
                                        changesetIndex.addChangeset(changeset, repositoryService.getById(repositoryId));
                                        return null;
                                    }
                                });
                            }
                            changesetIndex.addAttribute(changeset.getId(), PROCESSED, String.valueOf(true));
                        } else {
                            failed.put(issueKey, assignee);
                        }
                    }

                    return true;
                }

                @Override
                public void onEnd() throws IOException {
                    // no-op
                }

                @Override
                public void onStart() throws IOException {
                    // no-op
                }

            });
        }

        hookResponse.out().write("\n");
        
        if (!errorMessages.isEmpty()) {
            hookResponse.out().write("Oops! One or more JIRA directives from your commit messages failed:\n");
            for (String errorMessage : errorMessages) {
                hookResponse.out().write("\n\tError: " + errorMessage);
            }

            hookResponse.out().write("\n\nHere's a summary of the successful and failed operations:\n\n");
        }

        if (!assigned.isEmpty()) {
            for (Map.Entry<String, String> entry : assigned.entrySet()) {
                hookResponse.out().write(String.format("\tAssigned %s to %s\n", entry.getKey(), entry.getValue()));
            }
            hookResponse.out().write("\n");
        }


        if (!failed.isEmpty()) {
            for (Map.Entry<String, String> entry : failed.entrySet()) {
                hookResponse.out().write(String.format("\tFailed to assign %s to %s\n", entry.getKey(), entry.getValue()));
            }
            hookResponse.out().write(String.format(
                    "\n1.) To retry the above failed JIRA directives, simply re-push your changes.\n" +
                    "\n2.) To suppress JIRA directive processing for this push, either:\n" +
                    "\nVisit the following web page:\n" +
                    "\n\t%s\n" +
                    "\nor run the following command:\n" +
                    "\n\t%s\n" +
                    "\nand re-push your changes.\n\n",
                    applicationProperties.getBaseUrl() + "/plugins/servlet/jira-hook",
                    "curl -u " + username + " -X POST " + applicationProperties.getBaseUrl() + "/plugins/servlet/jira-hook?disable=once"));
            return false;
        }

        return true;
    }
}

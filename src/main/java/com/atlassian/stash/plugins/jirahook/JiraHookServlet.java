package com.atlassian.stash.plugins.jirahook;

import com.atlassian.sal.api.user.UserManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JiraHookServlet extends HttpServlet {

    private final JiraHookService jiraHookService;
    private final UserManager userManager;

    public JiraHookServlet(JiraHookService jiraHookService, UserManager userManager) {
        this.jiraHookService = jiraHookService;
        this.userManager = userManager;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = userManager.getRemoteUsername();
        if (username == null) {
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Must be authenticated.");
            return;
        }

        String disable = req.getParameter("disable");

        if (disable == null) {
            jiraHookService.enableFor(username);
        } else if ("once".equalsIgnoreCase(disable.trim())) {
            jiraHookService.disableOnceFor(username);
        } else {
            jiraHookService.disableFor(username);
        }

        resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getRemoteUser();
        if (username == null) {
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Must be authenticated.");
            return;
        }

        resp.setStatus(HttpServletResponse.SC_OK);
        String state = jiraHookService.isEnabled(username) ? "ENABLED" : "DISABLED";
        resp.getWriter().write(state);
    }

}

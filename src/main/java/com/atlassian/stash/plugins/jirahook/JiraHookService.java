package com.atlassian.stash.plugins.jirahook;

public interface JiraHookService {

    void enableFor(String username);

    void disableFor(String username);

    void disableOnceFor(String username);

    boolean isEnabled(String username);

}

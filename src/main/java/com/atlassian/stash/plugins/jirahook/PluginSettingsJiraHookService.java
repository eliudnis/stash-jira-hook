package com.atlassian.stash.plugins.jirahook;

import com.atlassian.event.api.EventListener;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.stash.event.RepositoryPushEvent;

public class PluginSettingsJiraHookService implements JiraHookService {

    private static final Mode DEFAULT_MODE = Mode.ENABLED;

    private enum Mode {

        ENABLED(true),
        DISABLED_ONCE(false),
        DISABLED(false);

        private final boolean enabled;

        private Mode(boolean enabled) {
            this.enabled = enabled;
        }

        public boolean isEnabled() {
            return enabled;
        }
    }

    private final PluginSettingsFactory pluginSettingsFactory;

    public PluginSettingsJiraHookService(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    @Override
    public void enableFor(String username) {
        set(username, Mode.ENABLED);
    }

    @Override
    public void disableFor(String username) {
        set(username, Mode.DISABLED);
    }

    @Override
    public void disableOnceFor(String username) {
        if (isEnabled(username)) {
            set(username, Mode.DISABLED_ONCE);
        }
    }

    @Override
    public boolean isEnabled(String username) {
        return get(username).isEnabled();
    }

    @SuppressWarnings("unused")
    @EventListener
    public void onPush(RepositoryPushEvent event) {
        if (event.getUser() == null) {
            return;
        }

        String name = event.getUser().getName();
        if (get(name) == Mode.DISABLED_ONCE) {
            set(name, Mode.ENABLED);
        }
    }

    private Mode get(String username) {
        String name = (String) settings().get(makeKey(username));
        return name != null ? Mode.valueOf(name) : DEFAULT_MODE;
    }

    private void set(String username, Mode mode) {
        settings().put(makeKey(username), mode.name());
    }

    private PluginSettings settings() {
        return pluginSettingsFactory.createGlobalSettings();
    }

    private String makeKey(String username) {
        return "jira.hook." + username + ".enabled";
    }

}

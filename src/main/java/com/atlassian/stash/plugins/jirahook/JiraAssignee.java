package com.atlassian.stash.plugins.jirahook;

import com.atlassian.stash.rest.data.RestMapEntity;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class JiraAssignee extends RestMapEntity {

    public JiraAssignee(String name) {
        put("name", name);
    }

}

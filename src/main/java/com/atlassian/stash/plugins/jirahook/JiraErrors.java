package com.atlassian.stash.plugins.jirahook;

import com.atlassian.stash.rest.data.RestMapEntity;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;
import java.util.Map;

@JsonSerialize
public class JiraErrors extends RestMapEntity {

    // for Jersey
    public JiraErrors() {
    }

    @SuppressWarnings("unchecked")
    public Map<String, String> getErrors() {
        return (Map<String, String>) get("errors");
    }

    @SuppressWarnings("unchecked")
    public List<String> getErrorMessages() {
        return (List<String>) get("errorMessages");
    }

}
